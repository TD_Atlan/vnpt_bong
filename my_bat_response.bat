@echo off

cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\Lucky88_Response\RESULTS
del /s VNPT_mayman_report_response.txt
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\Lucky88_Response
call robot --outputdir C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\Lucky88_Response\RESULTS TESTS
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\Lucky88_Response\DATA
call python check_response_mayman.py

cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\One88_Response\RESULTS
del /s VNPT_18_report_response.txt
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\One88_Response
call robot --outputdir C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\One88_Response\RESULTS TESTS
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\One88_Response\DATA
call python check_response_18.py

cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\8Live_Response\RESULTS
del /s VNPT_conlai_report_response.txt
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\8Live_Response
call robot --outputdir C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\8Live_Response\RESULTS TESTS
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\8Live_Response\DATA
call python check_response_conlai.py

cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\388Bet_Response\RESULTS
del /s VNPT_tambet_report_response.txt
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\388Bet_Response
call robot --outputdir CC TESTS
cd C:\PythonAutomation\PROJECT_BONG\PROJECT_DAT\388Bet_Response\DATA
call python check_response_tambet.py

exit